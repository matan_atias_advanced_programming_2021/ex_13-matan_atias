#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include "Helper.h"
#include <map>
#include <fstream>

using std::string;


class Server
{
	public:
		Server();
		~Server();
		void serve(int port);
	private:
		std::vector<std::thread> _threads;
		std::map<std::string, SOCKET> _clients;
		int _clientCount = 0;
		void accept();
		void clientHandler(SOCKET clientSocket);
		string getUsers();
		string getUsername(SOCKET clientSocket);
		SOCKET _serverSocket;
		void writeToFile(std::ofstream& file, string message, string author);
};

