#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <mutex>
#include <fstream>


using std::cout;
using std::endl;
using std::string;


std::mutex threadLock;
std::mutex fileLock;


Server::Server()
{
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 
	if (_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}


Server::~Server()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}


void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };
	//declaring vars
	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;
	cout << "Starting..." << endl;
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
	cout << "binded" << endl;
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	cout << "listening..." << endl;
	while (true)
	{
		std::cout << "accepting client..." << std::endl;
		accept();
	}
}


void Server::accept()
{
	threadLock.lock();
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}
	std::cout << "Client accepted !" << std::endl;
	this->_threads.push_back(std::thread(&Server::clientHandler, this, client_socket));
	this->_clientCount++;
	threadLock.unlock();
}


void Server::clientHandler(SOCKET clientSocket)
{
	try
	{
		while (true)
		{
			int messageCode = Helper::getMessageTypeCode(clientSocket);
			int len = 0;
			string message;
			string username;
			string fileName;
			string fileContent;
			string line;
			std::ofstream writeMessagesFile;
			std::ifstream readMessagesFile;
			//declaring vars
			if (messageCode == MT_CLIENT_LOG_IN)
			{
				len = Helper::getIntPartFromSocket(clientSocket, 2); //username length
				username = Helper::getStringPartFromSocket(clientSocket, len); //username
				this->_clients.insert(std::pair<string, SOCKET>(username, clientSocket));
				cout << "ADDED new client " << clientSocket << ", " << username << " to clients list" << endl;
				Helper::send_update_message_to_client(clientSocket, "", "", getUsers());
				for (auto it = this->_clients.begin(); it != this->_clients.end(); ++it)
				{
					Helper::send_update_message_to_client(it->second, "", "", getUsers());
				}
			}
			else if (messageCode == MT_CLIENT_UPDATE)
			{
				len = Helper::getIntPartFromSocket(clientSocket, 2); //other username length
				username = Helper::getStringPartFromSocket(clientSocket, len); //username to send message
				len = Helper::getIntPartFromSocket(clientSocket, 5); //message len
				message = Helper::getStringPartFromSocket(clientSocket, len); //message
				fileName += username;
				fileName += "&";
				fileName += getUsername(clientSocket);
				fileName += ".txt";
				fileLock.lock();
				readMessagesFile.open(fileName);
				if (!readMessagesFile.is_open())
				{
					fileName.clear();
					fileName += getUsername(clientSocket);
					fileName += "&";
					fileName += username;
					fileName += ".txt";
				}
				else
				{
					readMessagesFile.close();
				}
				if (len != 0)
				{
					writeMessagesFile.open(fileName, std::ios_base::app);
					if (writeMessagesFile.is_open())
					{
						writeToFile(writeMessagesFile, message, getUsername(clientSocket));
						writeMessagesFile.close();
					}
				}
				readMessagesFile.open(fileName);
				if (readMessagesFile.is_open())
				{
					while (std::getline(readMessagesFile, line))
					{
						fileContent += line;
					}
					readMessagesFile.close();
				}
				fileLock.unlock();
				Helper::send_update_message_to_client(clientSocket, fileContent, username, getUsers());
				fileName.clear();
			}
		}
	}
	catch (const std::exception& e)
	{
		this->_clients.erase(getUsername(clientSocket)); //removing user from list
		cout << "Exception was catch in function clientHandler. socket=" << clientSocket << ", what=" << e.what() << endl;
		closesocket(clientSocket);
	}
}


/*
	Function goes over all the map and returns string of all usernames with '&' between each username
	Input:
		None
	Output:
		string of all usernames with '&' between each username
*/
string Server::getUsers()
{
	string usernameList;
	//declaring vars
	for (auto it = this->_clients.begin(); it != this->_clients.end(); ++it)
	{
		usernameList += it->first;
		usernameList += "&";
	}
	usernameList = usernameList.substr(0, usernameList.size() - 1);
	return usernameList;
}


/*
	Function returns username using the client's socket
	Input:
		client's socket
	Output:
		client's username
*/
string Server::getUsername(SOCKET clientSocket)
{
	string username;
	//declaring vars
	for (auto it = this->_clients.begin(); it != this->_clients.end(); ++it)
	{
		if (it->second == clientSocket)
		{
			return it->first;
		}
	}
}


/*	
	Function writes into a file
	Input:
		file, message
	Output:
		None
*/
void Server::writeToFile(std::ofstream& file, string message, string author)
{
	string fullMessage;
	//declaring vars
	fullMessage += "&MAGSH_MESSAGE&&Author&";
	fullMessage += author;
	fullMessage += "&DATA&";
	fullMessage += message;
	file << fullMessage;
}