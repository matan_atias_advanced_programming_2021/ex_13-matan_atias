#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>


using std::cout;
using std::endl;
using std::string;


int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;
		myServer.serve(8826);
	}
	catch (std::exception& e)
	{
		cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}